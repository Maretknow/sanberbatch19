<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp_code extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','otp_name','user_id'
    ];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /* 
    * get the user record with otp
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
